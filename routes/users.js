var express = require("express");
var router = express.Router();
const bcrypt = require("bcrypt");
const moment = require("moment");
const jwt = require("jsonwebtoken");
const { createUserSchema, loginSchema } = require("../utils/schema");
const db = require("../db");

/**
 * @swagger
 * tags:
 *   name: User
 *   description: User Management
 */

/**
 * @swagger
 * /users:
 *   post:
 *     summary: Create user
 *     tags: [User]
 *     requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                email:
 *                  type: string
 *                  description: The email address of the user.
 *                  example: user@example.com
 *                password:
 *                  type: string
 *                  description: The password of the user.
 *                  example: password123
 *                user_name:
 *                  type: string
 *                  description: The username of the user.
 *                  example: testing
 *              required:
 *                - email
 *                - password
 *                - user_name
 *     responses:
 *       200:
 *         description: User created successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 user:
 *                   type: integer
 *                   description: The unique ID of the created user.
 *                   example: 23
 */
router.post("/", async function (req, res, next) {
  try {
    const { user_name, email, password } = await createUserSchema.validate(
      req.body
    );

    // check email exist
    const checkEmailExist = await db.getUserByEmail(email);
    if (checkEmailExist) {
      return res
        .status(500)
        .json({ message: `電郵地址已存在，請輸入其他電郵地址！` });
    }

    // hash the password before save to db
    const hashedPassword = await bcrypt.hash(password, 10);

    const userID = await db.createData(
      {
        user_name,
        email,
        password: hashedPassword,
        role_id: 2, // normal user, not admin
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "users"
    );

    res.status(200).json({ user_id: userID });
  } catch (err) {
    res.status(500).json({
      message: err && err.message ? err.message : "系統出現錯誤,請稍後再試",
    });
  }
});

/**
 * @swagger
 * /users/login:
 *   post:
 *     summary: Login user
 *     tags: [User]
 *     requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                email:
 *                  type: string
 *                  description: The email address of the user.
 *                  example: user@example.com
 *                password:
 *                  type: string
 *                  description: The password of the user.
 *                  example: password123
 *              required:
 *                - email
 *                - password
 *     responses:
 *       200:
 *         description: User created successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 user:
 *                   type: object
 *                   properties:
 *                     user_id:
 *                       type: integer
 *                       description: The unique ID of the created user.
 *                       example: 22
 *                     email:
 *                       type: string
 *                       description: The email address of the user.
 *                       example: user@example.com
 *                     user_name:
 *                       type: string
 *                       description: The username of the user.
 *                       example: testing
 *                     role_id:
 *                       type: integer
 *                       description: The ID of the user's role.
 *                       example: 2
 *                 token:
 *                   type: string
 *                   description: A token for authenticating the user's requests.
 */
router.post("/login", async function (req, res, next) {
  try {
    const { email, password } = await loginSchema.validate(req.body);
    const user = await db.getUserByEmail(email);
    if (!user) {
      return res.status(500).json({ message: `找不到相關的用戶` });
    }
    // compare user input password and db password
    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (!isPasswordValid) {
      return res.status(500).json({ message: `密碼不正確，請重新再試` });
    }

    const token = jwt.sign(
      { user_id: user.user_id, role_id: user.role_id },
      process.env.TOKEN_SECRET,
      {
        expiresIn: process.env.TOKEN_EXPIRY,
      }
    );
    // delete password field before pass to client side
    delete user["password"];
    res.json({ user, token });
  } catch (err) {
    res.status(500).json({
      message: err && err.message ? err.message : "系統出現錯誤,請稍後再試",
    });
  }
});

module.exports = router;
