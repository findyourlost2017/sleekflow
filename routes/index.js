var express = require("express");
var router = express.Router();
const { expressjwt: jwt } = require("express-jwt");
const _ = require("lodash");
const moment = require("moment");
const db = require("../db");
const {
  fetchTodoSchema,
  createTodoSchema,
  todoIdSchema,
  todoSwapPositionSchema,
} = require("../utils/schema");
const appConfig = require("../utils/appConfig");

/**
 * @swagger
 * /todos:
 *   get:
 *     summary: Returns all todos
 *     tags: [Todos]
 *     parameters:
 *       - $ref: '#/components/parameters/statusParam'
 *       - $ref: '#/components/parameters/dueDateFromParam'
 *       - $ref: '#/components/parameters/dueDateToParam'
 *       - $ref: '#/components/parameters/sortByParam'
 *       - $ref: '#/components/parameters/orderParam'
 *     responses:
 *       200:
 *         description: the list of the todos
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  todos:
 *                     type: array
 *                     items:
 *                        $ref: '#/components/schemas/Todo'
 *     security:
 *       - bearerAuth: []
 */
router.get(
  "/todos",
  jwt({
    secret: process.env.TOKEN_SECRET,
    credentialsRequired: true,
    algorithms: ["HS256"],
  }),
  async function (req, res, next) {
    try {
      const { status, due_date_from, due_date_to, sort_by, order } =
        await fetchTodoSchema.validate(req.query);

      const todos = await db.getTodoList(
        status,
        due_date_from,
        due_date_to,
        sort_by,
        order
      );

      if (todos && todos.length > 0) {
        for (const item of todos) {
          item.status = appConfig.statusMap[item.status];
          item.due_date = moment(item.due_date).format("YYYY-MM-DD");
          item.tags = _.map(await db.getTodoTagsByID(item.todo_id), "name");
        }
      }

      res.json({
        todos,
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        message: err && err.message ? err.message : "系統出現錯誤,請稍後再試",
      });
    }
  }
);

/**
 * @swagger
 * /todos/{todo_id}:
 *   get:
 *     summary: get todo item by ID
 *     tags: [Todos]
 *     parameters:
 *       - in: path
 *         name: todo_id
 *         schema:
 *           type: integer
 *         required: true
 *         description: The ID of the todo item to retrieve
 *     responses:
 *       200:
 *         description: the list of the todos
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  todo:
 *                    $ref: '#/components/schemas/Todo'
 *     security:
 *       - bearerAuth: []
 */
router.get(
  "/todos/:todo_id",
  jwt({
    secret: process.env.TOKEN_SECRET,
    credentialsRequired: true,
    algorithms: ["HS256"],
  }),
  async function (req, res, next) {
    try {
      // fetch todo item by id
      const todoItem = await db.getTodoByID(req.params.todo_id);

      // show error message when id incorrect
      if (!todoItem) {
        return res.sendStatus(404);
      }

      // fetch todo tags by id and convert array object to array
      const todoTags = _.map(
        await db.getTodoTagsByID(todoItem.todo_id),
        "name"
      );

      res.json({
        todo: {
          ...todoItem,
          tags: todoTags,
        },
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        message: err && err.message ? err.message : "系統出現錯誤,請稍後再試",
      });
    }
  }
);

/**
 * @swagger
 * /todos:
 *   post:
 *     summary: Create todo
 *     tags: [Todos]
 *     requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                  description: The name of the todo.
 *                  example: todo name
 *                description:
 *                  type: string
 *                  description: The description of the todo.
 *                  example: description
 *                due_date:
 *                  type: string
 *                  format: date
 *                  description: The due date of the todo.
 *                status:
 *                  type: integer
 *                  description: The status of the todo.
 *                  schema:
 *                    enum:
 *                      - 0
 *                      - 1
 *                      - 2
 *              required:
 *                - name
 *                - due_date
 *     responses:
 *       200:
 *         description: User created successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 user:
 *                   type: integer
 *                   description: The unique ID of the created user.
 *                   example: 23
 *     security:
 *       - bearerAuth: []
 */
router.post(
  "/todos",
  jwt({
    secret: process.env.TOKEN_SECRET,
    credentialsRequired: true,
    algorithms: ["HS256"],
  }),
  async function (req, res, next) {
    try {
      const { name, description, due_date, status, tags } =
        await createTodoSchema.validate(req.body);

      const maxPriority = await db.getMaxPriority();

      // create todo item
      const todoID = await db.createData(
        {
          name,
          description,
          due_date: moment(due_date).format("YYYY-MM-DD"),
          status: status, // normal user, not admin
          priority: maxPriority, // default set priority to zero
          created_by: req.auth.user_id,
          created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
        },
        "todo"
      );

      // create todo tags item
      if (tags && tags.length > 0) {
        for (const tagName of tags) {
          await db.createData(
            {
              name: tagName,
              todo_id: todoID,
            },
            "tags"
          );
        }
      }

      res.json({
        todo_id: todoID,
        name,
        description,
        due_date: moment(due_date).format("YYYY-MM-DD"),
        status: status, // normal user, not admin
        priority: maxPriority, // default set priority to zero
        created_by: req.auth.user_id,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
        tags,
      });
    } catch (err) {
      res.status(500).json({
        message: err && err.message ? err.message : "系統出現錯誤,請稍後再試",
      });
    }
  }
);

// UPDATE TODO
router.put(
  "/todos/:todo_id",
  jwt({
    secret: process.env.TOKEN_SECRET,
    credentialsRequired: true,
    algorithms: ["HS256"],
  }),
  async function (req, res, next) {
    try {
      const todoID = await todoIdSchema.validate(req.params.todo_id);
      const { name, description, due_date, status, tags } =
        await createTodoSchema.validate(req.body);

      // Step1 : Update the TODO item with the specified ID in the database
      const affectedTodoRows = await db.updateTodoByID(todoID, {
        name,
        description,
        due_date,
        status,
        updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      });
      // check update affect count, it must be bigger than 0
      if (affectedTodoRows === 0) {
        return res.sendStatus(404);
      }

      // Step2: Delete all tags
      await db.clearTodoTags(todoID);

      // Step3: Insert new tags
      if (tags && tags.length > 0) {
        for (const tagName of tags) {
          await db.createData(
            {
              name: tagName,
              todo_id: todoID,
            },
            "tags"
          );
        }
      }

      res.json({
        todo_id: todoID,
        name,
        description,
        due_date,
        status: appConfig.statusMap["status"],
        tags,
      });
    } catch (err) {
      res.status(500).json({
        message: err && err.message ? err.message : "系統出現錯誤,請稍後再試",
      });
    }
  }
);

// SWAP TODO priority, suppose drag adn drop to switch the position
router.post(
  "/todos/swap",
  jwt({
    secret: process.env.TOKEN_SECRET,
    credentialsRequired: true,
    algorithms: ["HS256"],
  }),
  async function (req, res, next) {
    try {
      const { source_id, target_id } = await todoSwapPositionSchema.validate(
        req.body
      );

      if (source_id === target_id) {
        return res.status(500).json({
          message: "請重新調整TODO位置",
        });
      }

      const sourceTodo = await db.getTodoByID(source_id);
      const targetTodo = await db.getTodoByID(target_id);

      if (!sourceTodo || !targetTodo) {
        return res.status(500).json({
          message: "記錄可能已被其他人刪除",
        });
      }

      await db.updateTodoByID(source_id, {
        priority: targetTodo.priority,
      });

      await db.updateTodoByID(target_id, {
        priority: sourceTodo.priority,
      });

      res.sendStatus(200);
    } catch (err) {
      console.log(err);
      res.status(500).json({
        message: err && err.message ? err.message : "系統出現錯誤,請稍後再試",
      });
    }
  }
);

// DELETE TODO
router.delete(
  "/todos/:todo_id",
  jwt({
    secret: process.env.TOKEN_SECRET,
    credentialsRequired: true,
    algorithms: ["HS256"],
  }),
  async function (req, res, next) {
    try {
      const todoID = await todoIdSchema.validate(req.params.todo_id);
      if (!req.auth.role_id) {
        return res.status(500).json({
          message: "系統出現錯誤,請聯絡IT",
        });
      }

      // check delete permission
      const permission = await db.getUserRolePermission(req.auth.role_id);
      if (!permission) {
        // If db not set the role permission
        return res.status(500).json({
          message: "系統出現錯誤,請聯絡IT",
        });
      }

      if (permission && permission.delete !== "Y") {
        return res.status(500).json({
          message: "你沒有權限刪除這個TODO ITEM",
        });
      }

      const affectedTodoRows = await db.updateTodoByID(todoID, {
        status: appConfig.statusNameMap["DELETED"],
        updated_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      });

      if (affectedTodoRows === 0) {
        return res.sendStatus(404);
      }

      res.json({ success: true });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        message: err && err.message ? err.message : "系統出現錯誤,請稍後再試",
      });
    }
  }
);

module.exports = router;
