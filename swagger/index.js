const swaggerJsdoc = require("swagger-jsdoc");
const { version } = require("../package.json");

const swaggerOptions = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Sleekflow API",
      version,
      description: "A description of TODO API",
    },
    servers: [
      {
        url: process.env.DOMAIN,
        description: "Sleekflow TODO API Documentation",
      },
    ],
  },
  apis: ["./routes/*.js", "./swagger/parameters.yaml"],
};

const swaggerSpec = swaggerJsdoc(swaggerOptions);

module.exports = swaggerSpec;
