const moment = require("moment");
const mysqlDB = require("../connection");
const appConfig = require("../utils/appConfig");

module.exports = {
  createData: async (data, table_name) => {
    return new Promise(function (resolve, reject) {
      let sqlQuery = "INSERT INTO " + table_name + "  SET ?";
      mysqlDB(sqlQuery, data, function (err, result) {
        if (err) {
          console.log("data  :  ", data);
          console.log("table_name  :  ", table_name);
          console.log("createData   :  ", err);
          reject({ code: 500, message: "系統出現錯誤，請稍後再試" });
        } else {
          resolve(result.insertId);
        }
      });
    });
  },
  getUserByEmail: async (email) => {
    return new Promise(function (resolve, reject) {
      let sqlQuery =
        "SELECT user_id, email, password, user_name, role_id from users";
      sqlQuery += " WHERE email = ?";
      mysqlDB(
        sqlQuery,
        // filter by delete record
        [email],
        function (err, rows) {
          if (err) {
            console.log("getUserByEmail   err :  ", err);
            reject({ code: 500, message: "系統出現錯誤，請稍後再試" });
          } else if (rows && rows.length > 0) {
            resolve(rows[0]);
          } else {
            resolve(null);
          }
        }
      );
    });
  },
  getUserRolePermission: async (roleID) => {
    return new Promise(function (resolve, reject) {
      let sqlQuery = "SELECT `create`, `update`, `delete` FROM roles";
      sqlQuery += " WHERE role_id = ?";
      mysqlDB(sqlQuery, roleID, function (err, rows) {
        if (err) {
          console.log("getUserRolePermission   err :  ", err);
          reject({ code: 500, message: "系統出現錯誤，請稍後再試" });
        } else if (rows && rows.length > 0) {
          resolve(rows[0]);
        } else {
          resolve(null);
        }
      });
    });
  },
  getTodoList: async (status, dueDateFrom, dueDateTo, sortBy, order) => {
    return new Promise(function (resolve, reject) {
      const contents = [];
      let sqlQuery =
        "SELECT todo_id, name, description, status, due_date, priority FROM todo";
      sqlQuery += " WHERE 1 = 1";

      if (status !== null && status !== undefined) {
        sqlQuery += " AND status = ?";
        contents.push(status);
      } else {
        // Filter by deleted record
        sqlQuery += " AND status <= ?";
        contents.push(appConfig.statusNameMap["COMPLETED"]);
      }

      if (dueDateFrom) {
        sqlQuery += " AND due_date >= ?";
        contents.push(moment(dueDateFrom).format("YYYY-MM-DD"));
      }

      if (dueDateTo) {
        sqlQuery += " AND due_date <= ?";
        contents.push(moment(dueDateTo).format("YYYY-MM-DD"));
      }

      if (sortBy) {
        sqlQuery += ` ORDER BY ${sortBy}`;
        if (order) {
          sqlQuery += ` ${order}`;
        }
      } else {
        sqlQuery += ` ORDER BY priority desc`;
      }

      mysqlDB(sqlQuery, contents, function (err, rows) {
        if (err) {
          console.log("getTodoList   err :  ", err);
          reject({ code: 500, message: "系統出現錯誤，請稍後再試" });
        } else if (rows && rows.length > 0) {
          resolve(rows);
        } else {
          resolve([]);
        }
      });
    });
  },
  getTodoByID: async (todoID) => {
    return new Promise(function (resolve, reject) {
      let sqlQuery =
        "SELECT todo_id, name, description, due_date, priority, status FROM todo";
      sqlQuery += " WHERE todo_id = ? AND status != ?";
      mysqlDB(
        sqlQuery,
        [todoID, appConfig.statusNameMap["DELETED"]],
        function (err, rows) {
          if (err) {
            console.log("getTodoByID   err :  ", err);
            reject({ code: 500, message: "系統出現錯誤，請稍後再試" });
          } else if (rows && rows.length > 0) {
            resolve(rows[0]);
          } else {
            resolve(null);
          }
        }
      );
    });
  },
  getMaxPriority: async () => {
    return new Promise(function (resolve, reject) {
      let sqlQuery = "SELECT MAX(priority) as max_position FROM todo";
      mysqlDB(sqlQuery, null, function (err, rows) {
        if (err) {
          console.log("getMaxPriority   err :  ", err);
          reject({ code: 500, message: "系統出現錯誤，請稍後再試" });
        } else if (rows && rows.length > 0) {
          resolve(rows[0].max_position != null ? rows[0].max_position + 1 : 0);
        } else {
          resolve(0);
        }
      });
    });
  },
  getTodoTagsByID: async (todoID) => {
    return new Promise(function (resolve, reject) {
      let sqlQuery = "SELECT name FROM tags WHERE todo_id = ?";
      mysqlDB(sqlQuery, todoID, function (err, rows) {
        if (err) {
          console.log("getTodoTagsByID   err :  ", err);
          reject({ code: 500, message: "系統出現錯誤，請稍後再試" });
        } else if (rows && rows.length > 0) {
          resolve(rows);
        } else {
          resolve(null);
        }
      });
    });
  },
  updateTodoByID: async (todoID, data) => {
    return new Promise(function (resolve, reject) {
      let sqlQuery = "UPDATE todo SET ? WHERE todo_id = ?";
      mysqlDB(sqlQuery, [data, todoID], function (err, result) {
        if (err) {
          console.log("updateTodoByID err :  ", err);
          console.log("data :  ", data);
          console.log("todoID :  ", todoID);
          reject({ code: 500, message: "系統出現錯誤，請稍後再試" });
        } else {
          resolve(result.affectedRows);
        }
      });
    });
  },
  clearTodoTags: async (todoID) => {
    return new Promise(function (resolve, reject) {
      let sqlQuery = "DELETE FROM tags WHERE todo_id = ?";
      mysqlDB(sqlQuery, todoID, function (err, result) {
        if (err) {
          console.log("clearTodoTags err :  ", err);
          reject({ code: 500, message: "系統出現錯誤，請稍後再試" });
        } else {
          resolve(result.affectedRows);
        }
      });
    });
  },
};
