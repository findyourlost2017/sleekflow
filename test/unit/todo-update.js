require("dotenv").config();
const request = require("supertest");
const { expect } = require("chai");
const moment = require("moment");
const jwt = require("jsonwebtoken");
const db = require("../../db");
const app = require("../../app");

describe("todo update test case", () => {
  before(function () {
    console.log(" ===== Start Test ===== ");
  });

  after(function () {
    console.log(" ===== Finish Test ===== ");
  });

  it("should return 401 if no token is provided", async () => {
    const res = await request(app)
      .put("/todos/1")
      .send({
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0,
      });

    expect(res.status).to.equal(401);
  });

  it("should return 500 if name is not provided", async () => {
    // pre create todo
    const todoID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 0, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );

    const token = jwt.sign({ user_id: "1" }, process.env.TOKEN_SECRET);
    const res = await request(app)
      .put(`/todos/${todoID}`)
      .set("Authorization", `Bearer ${token}`)
      .send({
        description: "edit testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 1,
      });

    expect(res.status).to.equal(500);
  });

  it("should return 500 if due date is not provided", async () => {
    // pre create todo
    const todoID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 0, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );

    const token = jwt.sign({ user_id: "1" }, process.env.TOKEN_SECRET);
    const res = await request(app)
      .put(`/todos/${todoID}`)
      .set("Authorization", `Bearer ${token}`)
      .send({
        name: "edit testing",
        description: "edit testing",
        status: 1,
      });

    expect(res.status).to.equal(500);
  });

  it("should return 500 if Todo not found", async () => {
    const todoID = "123456789012345678901234"; // Invalid Todo ID
    const token = jwt.sign({ user_id: "1" }, process.env.TOKEN_SECRET);
    const res = await request(app)
      .put(`/todos/${todoID}`)
      .set("Authorization", `Bearer ${token}`)
      .send({
        name: "edit testing",
        description: "edit testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 1,
      });

    expect(res.status).to.equal(404);
  });

  it("should update a Todo with valid input and valid token", async () => {
    // pre create todo
    const todoID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 0, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );

    const token = jwt.sign({ user_id: "1" }, process.env.TOKEN_SECRET);
    const res = await request(app)
      .put(`/todos/${todoID}`)
      .set("Authorization", `Bearer ${token}`)
      .send({
        name: "edit testing",
        description: "edit testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 1,
      });

    expect(res.status).to.equal(200);
    expect(res.body).to.be.an("object");
    expect(res.body).to.have.property("todo_id");
    expect(res.body).to.have.property("name").equal("edit testing");
    expect(res.body).to.have.property("description").equal("edit testing");
  });
});
