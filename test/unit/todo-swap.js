require("dotenv").config();
const request = require("supertest");
const { expect } = require("chai");
const moment = require("moment");
const jwt = require("jsonwebtoken");
const db = require("../../db");
const app = require("../../app");

describe("todo swap position test case", () => {
  before(function () {
    console.log(" ===== Start Test ===== ");
  });

  after(function () {
    console.log(" ===== Finish Test ===== ");
  });

  it("should return 401 if no token is provided", async () => {
    // Pre create todo
    const todoID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 0, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );

    const res = await request(app).delete(`/todos/${todoID}`).send({});

    expect(res.status).to.equal(401);
  });

  it("should return 400 if invalid Todo ID is provided in body", async () => {
    // Pre create todo
    const todoOneID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 10, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );
    const invalidTodoId = "123456789012345678901234"; // Invalid Todo ID
    const token = jwt.sign(
      { user_id: 1, role_id: 1 },
      process.env.TOKEN_SECRET
    );

    const res = await request(app)
      .post(`/todos/swap`)
      .set("Authorization", `Bearer ${token}`)
      .send({
        source_id: todoOneID,
        target_id: invalidTodoId,
      });

    expect(res.status).to.equal(500);
    expect(res.body).to.be.an("object");
    expect(res.body)
      .to.have.property("message")
      .equal("記錄可能已被其他人刪除");
  });

  it("should return 400 if the same Todo ID is provided in body", async () => {
    // Pre create todo
    const todoOneID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 10, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );

    const token = jwt.sign(
      { user_id: 1, role_id: 1 },
      process.env.TOKEN_SECRET
    );

    const res = await request(app)
      .post(`/todos/swap`)
      .set("Authorization", `Bearer ${token}`)
      .send({
        source_id: todoOneID,
        target_id: todoOneID,
      });

    expect(res.status).to.equal(500);
    expect(res.body).to.be.an("object");
    expect(res.body).to.have.property("message").equal("請重新調整TODO位置");
  });

  it("should swap position of two Todos with valid IDs and valid token", async () => {
    // Pre create todo
    const todoOneID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 10, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );

    // Pre create todo two
    const todoTwoID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 15, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );

    const token = jwt.sign(
      { user_id: 1, role_id: 1 },
      process.env.TOKEN_SECRET
    );

    const res = await request(app)
      .post(`/todos/swap`)
      .set("Authorization", `Bearer ${token}`)
      .send({
        source_id: todoOneID,
        target_id: todoTwoID,
      });

    const todoOneAfterResult = await db.getTodoByID(todoOneID);
    const todoTwoAfterResult = await db.getTodoByID(todoTwoID);

    expect(res.status).to.equal(200);
    expect(todoOneAfterResult).to.have.property("priority").equal(15);
    expect(todoTwoAfterResult).to.have.property("priority").equal(10);
  });
});
