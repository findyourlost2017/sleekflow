require("dotenv").config();
const request = require("supertest");
const { expect } = require("chai");
const shortid = require("shortid");
const bcrypt = require("bcrypt");
const db = require("../../db");
const app = require("../../app");

describe("user register test case", () => {
  before(function () {
    console.log(" ===== Start Test ===== ");
  });

  after(function () {
    console.log(" ===== Finish Test ===== ");
  });

  it("should return 500 if email is empty", async () => {
    const res = await request(app).post("/users").send({
      password: "password",
    });
    expect(res.status).to.equal(500);
  });

  it("should return 500 if password is empty", async () => {
    const res = await request(app).post("/users").send({
      email: "example@example.com",
    });
    expect(res.status).to.equal(500);
  });

  it("should return 500 if email is already registered", async () => {
    const email = `${shortid.generate()}@gmail.com`;
    // create user
    await db.createData(
      {
        email,
        user_name: "testing",
        password: await bcrypt.hash("123456789", 10),
      },
      "users"
    );

    const res = await request(app).post("/users").send({
      email: email,
      user_name: "testing",
      password: "12345678",
      role_id: 2, //default 1: admin, 2: user
    });

    expect(res.status).to.equal(500);
  });

  it("should register a new user", async () => {
    const email = `${shortid.generate()}@gmail.com`;

    const res = await request(app).post("/users").send({
      email: email,
      user_name: "testing",
      password: "123456789",
      role_id: 2, //default 1: admin, 2: user
    });

    expect(res.status).to.equal(200);
    expect(res.body).to.have.property("user_id");
  });
});
