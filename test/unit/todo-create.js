require("dotenv").config();
const request = require("supertest");
const { expect } = require("chai");
const moment = require("moment");
const jwt = require("jsonwebtoken");
const app = require("../../app");

describe("todo create test case", () => {
  before(function () {
    console.log(" ===== Start Test ===== ");
  });

  after(function () {
    console.log(" ===== Finish Test ===== ");
  });

  it("should return 401 if no token is provided", async () => {
    const res = await request(app)
      .post("/todos")
      .send({
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0,
      });

    expect(res.status).to.equal(401);
  });

  it("should return 500 if name is not provided", async () => {
    const token = jwt.sign({ user_id: "1" }, process.env.TOKEN_SECRET);
    const res = await request(app)
      .post("/todos")
      .set("Authorization", `Bearer ${token}`)
      .send({
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 1,
      });
    expect(res.status).to.equal(500);
  });

  it("should return 500 if due date is not provided", async () => {
    const token = jwt.sign({ user_id: "1" }, process.env.TOKEN_SECRET);
    const res = await request(app)
      .post("/todos")
      .set("Authorization", `Bearer ${token}`)
      .send({
        name: "testing",
        description: "testing",
        status: 1,
      });
    expect(res.status).to.equal(500);
  });

  it("should create a new Todo", async () => {
    const token = jwt.sign({ user_id: "1" }, process.env.TOKEN_SECRET);
    const res = await request(app)
      .post("/todos")
      .set("Authorization", `Bearer ${token}`)
      .send({
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 1,
      });
    expect(res.status).to.equal(200);
    expect(res.body).to.be.an("object");
    expect(res.body).to.have.property("todo_id");
    expect(res.body).to.have.property("name").equal("testing");
  });
});
