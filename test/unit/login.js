require("dotenv").config();
const request = require("supertest");
const { expect } = require("chai");
const shortid = require("shortid");
const bcrypt = require("bcrypt");
const db = require("../../db");
const app = require("../../app");

describe("user login test case", () => {
  before(function () {
    console.log(" ===== Start Test ===== ");
  });

  after(function () {
    console.log(" ===== Finish Test ===== ");
  });

  it("should return 500 if email is empty", async () => {
    const res = await request(app).post("/users/login").send({
      password: "password",
    });
    expect(res.status).to.equal(500);
  });

  it("should return 500 if password is empty", async () => {
    const res = await request(app).post("/users/login").send({
      email: "example@gmail.com",
      password: "",
    });
    expect(res.status).to.equal(500);
  });

  it("should return 500 if password is invalid", async () => {
    const email = `${shortid.generate()}@gmail.com`;
    // create user
    await db.createData(
      {
        email,
        user_name: "testing",
        password: await bcrypt.hash("123456789", 10),
      },
      "users"
    );
    //  test user
    const res = await request(app).post("/users/login").send({
      email,
      password: "12345678",
    });
    expect(res.status).to.equal(500);
  });

  it("should return a valid token and user if email and password are correct", async () => {
    const email = `${shortid.generate()}@gmail.com`;
    // create user
    await db.createData(
      {
        email,
        user_name: "testing",
        password: await bcrypt.hash("123456789", 10),
        role_id: 2, //default 1: admin, 2: user
      },
      "users"
    );
    //  test user
    const res = await request(app).post("/users/login").send({
      email,
      password: "123456789",
    });
    expect(res.status).to.equal(200);
    expect(res.body).to.have.property("token");
    expect(res.body).to.have.property("user");
  });
});
