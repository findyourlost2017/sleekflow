require("dotenv").config();
const request = require("supertest");
const { expect } = require("chai");
const moment = require("moment");
const jwt = require("jsonwebtoken");
const app = require("../../app");
const db = require("../../db");

describe("todo get list test case", () => {
  before(function () {
    console.log(" ===== Start Test ===== ");
  });

  after(function () {
    console.log(" ===== Finish Test ===== ");
  });

  it("should return all todos for missing token", async () => {
    const response = await request(app).get("/todos");
    expect(response.status).to.equal(401);
  });

  it("should return a 404 error for an invalid id", async () => {
    const todoID = "123456789012345678901234"; // Invalid Todo ID
    const token = jwt.sign({ user_id: "1" }, process.env.TOKEN_SECRET);
    const response = await request(app)
      .get(`/todos/${todoID}`)
      .set("Authorization", `Bearer ${token}`);

    expect(response.status).to.equal(404);
  });

  it("should return all todos for valid user", async () => {
    const token = jwt.sign({ user_id: "1" }, process.env.TOKEN_SECRET);
    const response = await request(app)
      .get("/todos")
      .set("Authorization", `Bearer ${token}`);
    expect(response.status).to.equal(200);
    expect(response.body).to.be.an("object");
    expect(response.body).to.be.property("todos");
    expect(response.body.todos).to.be.an("array");
  });

  it("should return the todo with the given id", async () => {
    const todoID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 0, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );

    const token = jwt.sign({ user_id: "1" }, process.env.TOKEN_SECRET);
    const response = await request(app)
      .get(`/todos/${todoID}`)
      .set("Authorization", `Bearer ${token}`);

    expect(response.status).to.equal(200);
    expect(response.body).to.be.an("object");
  });
});
