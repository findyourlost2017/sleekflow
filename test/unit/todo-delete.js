require("dotenv").config();
const request = require("supertest");
const { expect } = require("chai");
const moment = require("moment");
const jwt = require("jsonwebtoken");
const db = require("../../db");
const app = require("../../app");

describe("todo delete test case", () => {
  before(function () {
    console.log(" ===== Start Test ===== ");
  });

  after(function () {
    console.log(" ===== Finish Test ===== ");
  });

  it("should return 401 if no token is provided", async () => {
    // Pre create todo
    const todoID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 0, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );

    const res = await request(app).delete(`/todos/${todoID}`).send({});

    expect(res.status).to.equal(401);
  });

  it("should return 500 if no permission", async () => {
    // Pre create todo
    const todoID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 0, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );

    const token = jwt.sign(
      { user_id: 1, role_id: 2 },
      process.env.TOKEN_SECRET
    );

    const res = await request(app)
      .delete(`/todos/${todoID}`)
      .set("Authorization", `Bearer ${token}`);

    expect(res.status).to.equal(500);
  });

  it("should return 404 if Todo not found", async () => {
    const todoId = "123456789012345678901234"; // Invalid Todo ID
    const token = jwt.sign(
      { user_id: "1", role_id: 1 },
      process.env.TOKEN_SECRET
    );

    const res = await request(app)
      .delete(`/todos/${todoId}`)
      .set("Authorization", `Bearer ${token}`);

    expect(res.status).to.equal(404);
  });

  it("should delete a Todo with valid ID and valid token", async () => {
    // Pre create todo
    const todoID = await db.createData(
      {
        name: "testing",
        description: "testing",
        due_date: moment().add(1, "d").format("YYYY-MM-DD"),
        status: 0, // normal user, not admin
        priority: 0, // default set priority to zero
        created_by: 1,
        created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
      "todo"
    );

    const token = jwt.sign(
      { user_id: 1, role_id: 1 },
      process.env.TOKEN_SECRET
    );

    const res = await request(app)
      .delete(`/todos/${todoID}`)
      .set("Authorization", `Bearer ${token}`);

    const findTodoRes = await request(app)
      .get(`/todos/${todoID}`)
      .set("Authorization", `Bearer ${token}`);

    expect(res.status).to.equal(200);
    expect(findTodoRes.status).to.equal(404);
  });
});
