const mysql = require("mysql");

const pool = mysql.createPool({
  host: "localhost",
  user: "root",
  password: process.env.NODE_ENV == "DEV" ? "" : "G97LFDUxQ5YnGfWhumxne4Cx",
  database: "todo",
  charset: "utf8mb4",
  multipleStatements: true,
});

const query = function (sql, options, callback) {
  if (typeof options === "function") {
    callback = options;
    options = undefined;
  }
  pool.getConnection(function (err, conn) {
    if (err) {
      callback(err, null, null);
    } else {
      conn.query(sql, options, function (err, results, fields) {
        callback(err, results, fields);
      });
      conn.release();
    }
  });
};

module.exports = query;
