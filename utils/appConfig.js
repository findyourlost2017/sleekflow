module.exports = {
  statusMap: {
    0: "Not Started",
    1: "In Progress",
    2: "Completed",
    3: "Deleted",
  },
  statusNameMap: {
    NOT_STARTED: 0,
    IN_PROGRESS: 1,
    COMPLETED: 2,
    DELETED: 3,
  },
};
