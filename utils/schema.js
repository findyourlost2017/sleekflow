const yup = require("yup");

const fetchTodoSchema = yup.object({
  status: yup.number().oneOf([0, 1, 2]),
  due_date_from: yup.date(),
  due_date_to: yup.date(),
  sort_by: yup.string().oneOf(["status", "due_date", "name"]),
  order: yup.string().oneOf(["asc", "desc"]),
});

const createUserSchema = yup.object({
  user_name: yup.string().max(55).required("用戶名稱必須輸入"),
  email: yup.string().email().max(255).required("電郵地址必須輸入"),
  password: yup.string().required("密碼必須輸入").min(8),
});

const loginSchema = yup.object({
  email: yup.string().email().required(),
  password: yup.string().required(),
});

const createTodoSchema = yup.object().shape({
  name: yup.string().max(100).required("必須輸入TODO 的名稱"),
  description: yup.string().max(255),
  due_date: yup
    .date()
    .required("必須輸入TODO 的過期日")
    .min(new Date(Date.now() - 86400000), "過期日必須今天或之後"),
  status: yup.number().oneOf([0, 1, 2]).required("請先選擇TODO的狀態"),
  tags: yup.array().of(yup.string()),
});

const todoIdSchema = yup
  .number()
  .positive()
  .integer()
  .required("Todo ID 是必要輸入的field");

const todoSwapPositionSchema = yup.object({
  source_id: yup
    .number()
    .positive()
    .integer()
    .required("Source 是必要輸入的field"),
  target_id: yup
    .number()
    .positive()
    .integer()
    .required("Target 是必要輸入的field"),
});

module.exports = {
  createUserSchema,
  loginSchema,
  fetchTodoSchema,
  createTodoSchema,
  todoIdSchema,
  todoSwapPositionSchema,
};
